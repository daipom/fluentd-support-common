= Fluent Package LTS

fluent-package v5 LTSのリリースについて紹介するスライドです。

== 作者向け

=== 表示

  rake

=== PDF作成

  rake pdf

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide--lts-release-announce

=== 表示

  rabbit rabbit-slide--lts-release-announce.gem

