= Fluent Package LTS

# : subtitle
#    サブタイトル
# : author
#    作者
: institution
   株式会社クリアコード
# : content-source
#    イベント名
: date
   2023-09-26
: allotted-time
   30m
# : start-time
#    2023-09-21T14:00:14+09:00
# : end-time
#    2023-09-21T14:05:14+09:00
: theme
   clear-code

= 株式会社クリアコード

  # image
  # src = images/clearcode.svg
  # relative_width = 90

(('tag:center'))自由ソフトウェアとビジネス\nを両立！

== プロパティー

: enable-title-on-image
   false

= クリアコードとFluentd

  * Fluentdメンテナー3名在籍\n
    (('note:2021年からコミュニティの中心となって開発に参加！'))
  * Fluentdのサポートサービスを提供\n
    (('note:((<URL:https://www.clear-code.com/services/fluentd-service.html>))'))\n
    (('note:導入・アップデート支援、機能追加、障害対応...'))

= Fluentdメンテナー3名在籍

  # image
  # src = images/maintainers.png
  # relative_width = 70

(('note:((<URL:https://github.com/fluent/fluentd/graphs/contributors?from=2021-01-01&to=2023-09-26&type=c>))'))

== プロパティー

: enable-title-on-image
   false

= 本資料のトピック

  * Fluent Package LTSとは
  * LTSへの移行
  * 主な変更点
  * LTS向けサポートサービス

= Fluent Package LTSとは

= Fluent Package LTS

  * 2023年8月にv5としてリリース
    * td-agent v4の後継パッケージ
  * 長期の安定運用におすすめ
    * セキュリティー、バグフィックスのみ
    * サポート期間を事前アナウンス

= td-agent

  * 元々は ((*Treasure Data*)) 主体
  * 現在は ((*コミュニティ*)) 主体

  # image
  # src = https://raw.githubusercontent.com/fluent/fluent-package-builder/v4.5.1/td-agent/msi/assets/icon.png
  # relative_width = 40

= Fluent Package

  * コミュニティ主体であることを\nパッケージ名に反映！

  # image
  # src = https://raw.githubusercontent.com/fluent/fluentd-docs-gitbook/1.0/images/logo/Fluentd_icon_square.png
  # relative_width = 35

= LTS版と通常版との違い

  # RT

  , サポート期間

  LTS版, 長期、事前アナウンス\n(およそ2年ごと)
  通常版, 不定期\n(td-agent相当)

= リリースサイクル

  # image
  # src = https://raw.githubusercontent.com/fluent/fluentd-website/master/public/images/blog/20230829_fluent-package-scheduled-lifecycle.png
  # relative_width = 100

(('note:((<URL:https://www.fluentd.org/blog/fluent-package-scheduled-lifecycle>))'))

== プロパティー

: enable-title-on-image
   false

= LTSへの\n移行

= LTS版へ移行するメリット

  * 長期間に渡り安心してアップデート可能！
  * メジャーアップデートを計画的に準備可能！

= 移行方法\n(from td-agent v4)

  * 基本的には通常のインストール方法でアップデート可能！
  * 場合によってはマイグレート作業が必要

= アップデート: Linux

  * 例: Red Hat / CentOS
      $ curl -fsSL \
      https://toolbelt.treasuredata.com/sh/install-redhat-fluent-package5-lts.sh | sh
  * RPM Package\n
    (('note:((<URL:https://docs.fluentd.org/installation/install-by-rpm>))'))
  * DEB Package\n
    (('note:((<URL:https://docs.fluentd.org/installation/install-by-deb>))'))

= アップデート: Windows

  # image
  # src = images/how-to-download-1.png
  # relative_width = 100

== プロパティー

: enable-title-on-image
   false

= アップデート: Windows

  # image
  # src = images/how-to-download-3.png
  # relative_height = 100

== プロパティー

: enable-title-on-image
   false

= アップデート: Windows

  # image
  # src = images/wizard-1.png
  # relative_height = 100

== プロパティー

: enable-title-on-image
   false

= アップデート: Windows

  # image
  # src = images/wizard-2.png
  # relative_height = 100

== プロパティー

: enable-title-on-image
   false

= アップデート: Windows

  # image
  # src = images/wizard-3.png
  # relative_height = 100

== プロパティー

: enable-title-on-image
   false

= 手動マイグレート

(('note: 場合によっては手動のマイグレート作業が必要'))

  * 一般のケース
  * Linuxのケース
  * Windowsのケース

= 手動マイグレート\n一般

  * 手動インストールしているプラグインがある\n
    (('note: => プラグインの再インストール'))
  * Fluentdのログファイルを監視している\n
    (('note: => ログファイルのパスの更新'))

= プラグインの再インストール

  * アップデート((*前*))に\nプラグインリストを確認

  $ td-agent-gem list | grep fluent-plugin*
  fluent-plugin-calyptia-monitoring (0.1.3)
  fluent-plugin-elasticsearch (5.3.0)
  fluent-plugin-flowcounter-simple (0.1.0)
    .
    .
    .

= プラグインの再インストール

  * アップデート((*後*))に\nプラグインリストを確認

  $ fluent-gem list | grep fluent-plugin*
  fluent-plugin-calyptia-monitoring (0.1.3)
  fluent-plugin-elasticsearch (5.3.0)
  fluent-plugin-flowcounter-simple (0.1.0)
    .
    .
    .

= プラグインの再インストール

  * 必要なプラグインをインストール

  $ sudo fluent-gem install fluent-plugin-...

= ログファイルのパスの更新

  * Linux

      /var/log/td-agent/td-agent.log
        ⇩
      /var/log/fluent/fluentd.log

= ログファイルのパスの更新

  * Windows

      (インストールディレクトリ)/td-agent/td-agent.log
        ⇩
      (インストールディレクトリ)/fluent/fluentd.log

= 手動マイグレート\nLinux

次をカスタマイズしている場合

  * 環境変数設定ファイル
  * ローテート設定ファイル
  * ユニットファイル

= 環境変数設定ファイル

  * RHEL系

      /etc/sysconfig/td-agent
        ⇩
      /etc/sysconfig/fluentd

= 環境変数設定ファイル

  * Debian系

      /etc/default/td-agent
        ⇩
      /etc/default/fluentd

= ローテート設定ファイル

  /etc/logrotate.d/td-agent
    ⇩
  /etc/logrotate.d/fluentd

= カスタムユニットファイル

  * パッケージのパス構成が変化\n
    (('note: 例: /opt/td-agent/ => /opt/fluent/'))
  * カスタムユニットファイルの更新が必要（もし使っていれば）\n
    (('note: 例: /etc/systemd/system/td-agent.service'))

= 手動マイグレート\nWindows

  * サービス起動時コマンド引数をカスタマイズしている場合\n
    (('note: 例: ログローテート関連の引数を追加'))

= サービス起動時コマンド引数

  * アップデート((*前*))に確認

      $ REG QUERY HKLM\System\CurrentControlSet\Services\fluentdwinsvc

  * ((*fluentdopt*))の値を確認

= サービス起動時コマンド引数

  # image
  # src = images/fluentdopt-td-agent.png
  # relative_height = 100

== プロパティー

: enable-title-on-image
   false

= サービス起動時コマンド引数\nマイグレーション

  * アップデート後はデフォルト値になる
  * カスタマイズしていた場合は再設定が必要

= サービス起動時コマンド引数\nマイグレーション

(('note: 管理者権限のFluent Package Command Prompt'))

  $ fluentd --reg-winsvc-fluentdopt "(引数全体)"

= サービス起動時コマンド引数\nログローテート設定

  * ログローテートは((<"system"|URL:https://docs.fluentd.org/deployment/logging#log-rotation-setting>))設定を推奨
    * --log-rotate-age
    * --log-rotate-size

    (('note: td-agent v4.4.2以降はsystem設定で設定可能'))

= サービス起動時コマンド引数\n補足: デフォルト値の変化

  * 設定ファイルパス\n
    (('note: -c オプション'))

      -c (インストールディレクトリ)\td-agent\etc\td-agent\td-agent.conf
        ⇩
      -c (インストールディレクトリ)\fluent\etc\fluent\fluentd.conf

(('note:※ 旧設定ファイルは自動で新しいパスに移動'))

= サービス起動時コマンド引数\n補足: デフォルト値の変化

  * ログファイルパス\n
    (('note: -o オプション'))

      -o (インストールディレクトリ)\td-agent\td-agent.log
        ⇩
      -o (インストールディレクトリ)\fluent\fluentd.log

= 主な\n変更点

= 主な変更点

(('note: Fluent Package v5.0.0 および v5.0.1 について'))

  * パッケージの変更点
  * Fluentdの改善点

= パッケージの変更点

  * パッケージ名の変更\n
    (('note: td-agent => fluent-package'))
  * コマンド名の変更\n
    (('note: td-agent => fluentd'))\n
    (('note: td-agent-gem => fluent-gem'))\n
    (('note: マイグレート環境では旧名も使用可能'))
  * インストールパスの変更\n
    (('note: /opt/td-agent/ => /opt/fluent/'))

= パッケージの変更点\nLinux 1/4

  * サービス名\n
    (('note: td-agent => fluentd'))
  * 設定ファイルパス\n
    (('note: /etc/td-agent/td-agent.conf => /etc/fluent/fluentd.conf'))\n
    (('note: アップデート時に自動で再配置'))

= パッケージの変更点\nLinux 2/4

  * ログファイルパス\n
    (('note: /var/log/td-agent/td-agent.log => /var/log/fluent/fluentd.log'))
  * ログローテート設定パス\n
    (('note: /etc/logrotate.d/td-agent => /etc/logrotate.d/fluentd'))

= パッケージの変更点\nLinux 3/4

  * RHEL系
    * 環境変数設定ファイルパス\n
      (('note: /etc/sysconfig/td-agent => /etc/sysconfig/fluentd'))
    * ユニットファイルパス\n
      (('note: /usr/lib/systemd/system/td-agent.service =>'))\n
      (('note: /usr/lib/systemd/system/fluentd.service'))

= パッケージの変更点\nLinux 4/4

  * Debian系
    * 環境変数設定ファイルパス\n
      (('note: /etc/default/td-agent => /etc/default/fluentd'))
    * ユニットファイルパス\n
      (('note: /lib/systemd/system/td-agent.service =>'))\n
      (('note: /lib/systemd/system/fluentd.service'))

= パッケージの変更点\nWindows 1/2

  * コンソール名\n
    (('note: Td-agent Command Prompt => Fluent Package Command Prompt'))
  * 設定ファイルパス\n
    (('note: etc/td-agent/td-agent.conf => etc/fluent/fluentd.conf'))\n
    (('note: アップデート時に自動で再配置'))

= パッケージの変更点\nWindows 2/2

  * ログファイルパス\n
    (('note: td-agent.log => fluentd.log'))
  * FLUENT_PACKAGE_TOPDIR\n
    (('note: TD_AGENT_TOPDIRに代わる環境変数'))
  * インストール後に自動でサービスが起動しないように

= Fluentdの改善点

  * in_tailのfollow_inodes機能
  * Rubyの高速化機能（YJIT）
  * ignore_same_log_interval
  * 脆弱性対応

= in_tail\nfollow_inodesの動作改善

  * ローテートファイルも含めて収集対象に指定できる機能
  * 「ローテート後にファイルを収集しなくなる」不具合を改善

(('note:((<URL:https://docs.fluentd.org/input/tail#follow_inodes>))'))

= Rubyの高速化機能（YJIT）

  * YJITがサポートされている環境で利用可能\n
    (('note: Windowsは未サポート'))
  * Fluentdではまだ実験段階

(('note:((<URL:https://docs.fluentd.org/deployment/system-config#enable_jit-experimental>))'))

= ignore_same_log_interval

  * キャッシュが制限なく増加してメモリを圧迫することがある問題を修正

(('note:((<URL:https://docs.fluentd.org/deployment/logging#ignore_same_log_interval>))'))

= 脆弱性対応

  * ((<"CVE-2023-38697"|URL:https://github.com/socketry/protocol-http1/security/advisories/GHSA-6jwc-qr2q-7xwj>))\n
    * in_monitor_agentプラグインに影響

= LTS向け\nサポートサービス

= クリアコードの\nFluentdサポートサービス

  * 導入・アップデート支援、機能追加、障害対応...
  * LTS向けのサポートも開始！

= LTS向けサポートサービス

  * LTSの導入、アップデート支援\n
    (('note: 環境をヒアリングし、導入・アップデート手順をサポート'))
  * 脆弱性などの更新内容の通知・説明\n
    (('note: セキュリティフィックスの内容と影響範囲'))\n
    (('note: バグフィックスの内容と影響範囲'))

= お問い合わせ

  * サポートサービス\n
    (('note:((<URL:https://www.clear-code.com/services/fluentd-service.html>))'))
  * お問い合わせ\n
    (('note:((<URL:https://www.clear-code.com/contact/>))'))

(('note: お気軽にお問い合わせください！'))

= ぜひLTS版を\nご利用ください！
