# Fluentd Update 

subtitle
: Fluentdとパッケージの最新動向について

author
: Kentaro Hayashi, Daijiro Fukuda

institution
: Fluentd Project

content-source
: Open Source Conference 2024 Online/Spring

date
:   2024-03-01

allotted-time
:   40m

theme
: .

# 自己紹介

* Fluentdプロジェクトのメンテナー

![](images/how-active.png)

{:.center}

{::note}<https://github.com/fluent/fluentd/graphs/contributors?from=2022-01-01&to=2024-02-21&type=c>{:/note}
{::note}<https://github.com/fluent/fluent-package-builder/graphs/contributors?from=2022-01-01&to=2024-02-21&type=c>{:/note}

# 今回の発表内容

* Fluentdとは
* OSC2022 Online/Spring以降のできごと
* Fluent Package LTSとは
* Fluentd開発の最新トピック

# 今回の発表内容

* **Fluentdとは**
* OSC2022 Online/Spring以降のできごと
* Fluent Package LTSとは
* Fluentd開発の最新トピック

# Fluentdとは

> Fluentd is an open source data collector that unifies data collection and consumption.

{::comment}
See https://raw.githubusercontent.com/fluent/fluentd-docs-gitbook/1.0/images/logo/Fluentd_icon_square.png
{:/comment}

![](images/Fluentd_icon_square.png){:relative-width="20"}

# Fluentdのないログ収集の世界

{::comment}
See https://raw.githubusercontent.com/fluent/fluentd-website/master/public/images/fluentd-before.png
{:/comment}

![](images/fluentd-before.png){:relative-width="55"}

## Slide properties

enable-title-on-image
: false

# Fluentdのあるログ収集の世界

{::comment}
See https://raw.githubusercontent.com/fluent/fluentd-website/master/public/images/fluentd-architecture.png
{:/comment}

![](images/fluentd-architecture.png){:relative-width="100"}

## Slide properties

enable-title-on-image
: false

# Fluentdを選ぶ理由

* Flexibility
* Scalability
* Open source and community

# Flexibility

* 1000以上のプラグインを利用可能
* プラグインを組み合わせることで
  様々な用途に利用可能
* カスタムプラグインで拡張可能

# プラグインは随時追加されていく

* {::note}<https://www.fluentd.org/plugins/all>{:/note}
  * 定期的にプラグインのリストを更新中
  * 2/末時点で1164プラグイン
    {::note}直近2023/10以降だけでも+10増えた{:/note}

```
Refs: [master], {upstream/master}
Author:     ashie <ashie@users.noreply.github.com>
AuthorDate: Mon Feb 12 14:13:34 2024 +0000
Commit:     ashie <ashie@users.noreply.github.com>
CommitDate: Mon Feb 12 14:13:34 2024 +0000

    Update plugins.json

    added plugins:
      * fluent-plugin-kafka-status
```

# Scalability

![](images/scaling.png){:relative-width="75"}

## Slide properties

enable-title-on-image
: false

# Open source and community

* 開発へ参加できるよ
  * <https://github.com/fluent/fluentd>
* プラグインがコミュニティでメンテナンスされている
  * <https://github.com/fluent-plugins-nursery>

# 今回の発表内容

* Fluentdとは
* **OSC2022 Online/Spring以降のできごと**
* Fluent Package LTSとは
* Fluentd開発の最新トピック

# OSC2022での発表(1)

![](images/osc2022-presentation1.png){:relative-height="90"}

{::note}<https://slide.rabbit-shocker.org/authors/kenhys/osc2022-online-spring-fluentd/>{:/note}

# OSSを継続的にメンテナンスしていく仕組みづくり (OSC2022)

* パッケージをリリースするための仕組みを整備
* プラグインを引き取る仕組みの紹介
* よいフィードバックをもらうために工夫したことの紹介

# OSC2022での発表(2)

![](images/osc2022-presentation2.png){:relative-height="90"}

{::note}<https://slide.rabbit-shocker.org/authors/ashie/osc2022-online-spring-fluentd/>{:/note}

# Fluentdの最新情報 (OSC2022)

* 開発体制の変化
* v1.12〜v1.14のリリース内容の紹介
* 最新情報の入手方法を案内
* 今後の課題について

# 当時の発表内容のその後(1)

* フィードバックをもらう工夫としてGitHub Issue Templateを整備
  * ⇨サポートに必要な情報をもらいやすくなった😀
    {::note}定型的な項目を記入して報告してくれる世界{:/note}

# 当時の発表内容のその後(2)

* GitHub Discussionsでのコミュニティサポート
  * 自由記入欄なので最初から必要な前提条件を共有してもらいにくい😞
  * わかっている人でないと相互の助け合いがしにくい
    * ⇨ GitHub DiscussionsのQ&Aにフォームテンプレートを導入
  * Q&A(Japanese)カテゴリの新設

# Issue/Discussion テンプレート

![](images/github-template.png){:relative-width="86"}

{::comment}
relative-widthの値によって画像に枠線が入ってしまう。86なら入らない。
{:/comment}

# 当時の発表内容のその後(3)

* td-agentの名前を変更するかも
  * ⇨**fluent-package**に変更しました!
* LTS版を提供するかも
  * ⇨fluent-packageのLTS版提供をはじめました

# 当時の発表内容のその後(4)

* v1.15系のリリース
  * YAML形式の設定ファイルを許容 {::note}[GitHub#3712](https://github.com/fluent/fluentd/issues/3712){:/note}
  * out_fileの書き込み処理競合を修正 {::note}[GitHub#3808](https://github.com/fluent/fluentd/issues/3808){:/note}
* v1.16系のリリース
  * in_tailの不具合修正 {::note}[GitHub#4208](https://github.com/fluent/fluentd/issues/4208),[GitHub#4327](https://github.com/fluent/fluentd/issues/4327){:/note}
  * 強制電源断等によるファイル破損への対応の強化 {::note}[GitHub#3970](https://github.com/fluent/fluentd/issues/3970){:/note}

# Open Source Summit Japan 2023での発表

![](images/ossummitjapan2023-presentation.png){:relative-height="80"}

{::note}<https://slide.rabbit-shocker.org/authors/daipom/open-source-summit-japan-2023-fluent-package/>{:/note}

# OSSummit Open Source Summit Japan 2023での発表(2)

* Fluent Package LTSの紹介
* アップグレードするときの注意事項案内
* 主要な変更点の紹介

# 今回の発表内容

* Fluentdとは
* OSC2022 Online/Spring以降のできごと
* **Fluent Package LTSとは**
* Fluentd開発の最新トピック

# Fluent Package LTS

* 2023年8月にv5としてリリース
  * td-agent v4の後継パッケージ
  * 2023年12月にtd-agent v4.xはサポート終了
  {::note}<https://www.fluentd.org/blog/schedule-for-td-agent-4-eol>{:/note}
* 長期の安定運用におすすめ
  * セキュリティー、バグフィックスのみ
  * サポート期間を事前アナウンス

# td-agent

* 元々は **Treasure Data** 主体
* 現在は **コミュニティ** 主体

![](images/icon.png){:relative-height="90"}

# Fluent Package

* コミュニティ主体であることをパッケージ名に反映！

![](images/Fluentd_icon_square.png){:relative-height="90"}

# 通常版とLTS版との違い

|                      | サポート期間                |
|----------------------|-----------------------------|
| 通常版               | 約3ヶ月 \\n(td-agent相当)   |
| LTS (長期サポート版) | **約2年**\\n(事前アナウンス) |

# リリースサイクル

```mermaid
gantt
    title Scheduled Support lifecycle for Fluent Package
    dateFormat YYYY-MM
    axisFormat %Y-%m
    todayMarker off
    section v4

    %% date -d '20230508 113 days' +%Y%m%d
    %% => 20230829
    v4.5.0 :done, v450, 2023-05-08, 113d

    %% date -d '20230829 92 days' +%Y%m%d
    %% => 20231129
    v4.5.1 :done, v451, after v450, 92d

    %% date -d '20231129 32 days' +%Y%m%d
    %% => 20231231
    v4.5.2 :done, v452, after v451, 32d

    section v5.x
    %% date -d '20230729 31 days' +%Y%m%d
    %% => 20230829
    v5.0.0 :done, v500, 2023-07-29, 31d

    %% date -d '20230829 92 days' +%Y%m%d
    %% => 202350331
    v5.0.1 :done, v501, after v500, 92d
    v5.0.2 :active, v502, after v501, 92d
    %% date -d '20231129 92 days' +%Y%m%d
    %% => 20240229

    %% date -d '20240229 396 days' +%Y%m%d
    %% => 20250331
    v5.0.x (fluentd 1.16.x T.B.D.):v50x, after v502, 199d
    v5.1.x (fluentd 1.17.x T.B.D.):v51x, after v50x, 199d

    section v5.0 (LTS)
    v5.0.x (LTS fluentd 1.16.x) :lts_v5, 2023-07-29, 611d
    Fluent Package v6.0.x (LTS) :milestone, lts_v6, 2025-03-31, 10d
```
{:relative-height="90"}

{::note}<https://www.fluentd.org/blog/fluent-package-scheduled-lifecycle>{:/note}

# LTSへの移行

# LTS版へ移行するメリット

* 長期間に渡り安心してアップデート可能！
* メジャーアップデートを計画的に準備可能！

# td-agent v4からの移行方法

* 基本的には通常のインストール方法でアップデート可能！
* 場合によってはマイグレート作業が必要
* 移行方法はガイドを参考にしてね!
  {::note}<https://www.fluentd.org/blog/upgrade-td-agent-v4-to-v5>{:/note}
  * 日本語版もあるよ!
  {::note}<https://www.clear-code.com/blog/2024/1/24/upgrade-to-fluent-package-v5-from-v4.html>{:/note}

# プラグインの移行方法

* 独自に追加したものは再インストールが必要
  * fluent-diagtoolで検出できるよ(v1.0.3以降)
    {::note}td-agent v4では、まずツールをアップデート{:/note}
    {::note}Windowsでは、fluent-package v5.0.3から使えるようになる予定{:/note}

```
$ sudo td-agent-gem install fluent-diagtool
$ /opt/td-agent/bin/fluent-diagtool -t fluentd -o /tmp
...
[Diagtool] [INFO] [Collect] td-agent manually installed gem information is 
  stored in tmp/20230901075350/output/gem_local_list.output
[Diagtool] [INFO] [Collect] td-agent manually installed gems:
[Diagtool] [INFO] [Collect] * fluent-plugin-concat
...
```

# 今回の発表内容

* Fluentdとは
* OSC2022 Online/Spring以降のできごと
* Fluent Package LTSとは
* **Fluentd開発の最新トピック**

# 直近のリリース予定(1)

* Fluentd v1.16.4
  * チャンクのサイズ制限を超えたときに例外が発生することがある不具合を修正
    {::note}<https://github.com/fluent/fluentd/pull/4342>{:/note}
  * T.B.D.

# 直近のリリース予定(2)

* fluent package v5.0.3をリリース予定
  * Fluentd v1.16.4
  * Windowsにおける起動不具合の修正
  * td-agentからの移行処理の改善
  * ~~重複起動の防止~~ v5.0.4以降

# Windowsにおける起動不具合

* 環境によってはワーカープロセスを起動できない
  {::note}td-agent v4.5.0以降{:/note}
  {::note}<https://github.com/fluent/fluent-package-builder/issues/616>{:/note}
* Windowsサービスの開始がタイムアウトエラーになることがある
  {::note}fluent-package v5.0.0以降{:/note}
  {::note}<https://github.com/fluent/fluent-package-builder/issues/618>{:/note}

# td-agentからの移行処理改善

* rpm: サービスの自動起動設定を引き継げるように改善
  {::note}<https://github.com/fluent/fluent-package-builder/pull/613>{:/note}
* deb rpm: アップグレード時にファイル移行に失敗することがある不具合を修正
  {::note}空白文字などを含むファイルに対応{:/note}
  {::note}<https://github.com/fluent/fluent-package-builder/pull/615>{:/note}

# 重複起動の防止

* 誤操作による重複起動の防止策を実装予定
  {::note}<https://github.com/fluent/fluent-package-builder/issues/611>{:/note}
  * e.g. バージョン表示すべくサービス起動中に/usr/sbin/fluentd -vを実行してはダメ
    {::note}`-v`はデバッグログレベルの指定。`--version`が正しい{:/note}

# 今後の課題・展望(1)

* Fluentd
  * OpenTelemetry対応(プロトコル仕様の拡張) {::note}Forward Protocol Specification v1.5{:/note}
  * in_tailの機能改善と整備
  * ダウンタイムなしの設定リロードとアップデート
  * 2038年問題への対応

{::note}T.B.D. See <https://github.com/fluent/fluentd/milestone/13>{:/note}

# 今後の課題・展望(2)

* Fluent Package LTS
  * 継続的な開発・リリース
  * fluent-packageの配布インフラをCNCFプロジェクトにスポンサーしてもらう
* 持続可能なコミュニティサポート

# まとめ

* Fluentdの長期サポートが必要ならLTS版おすすめ！
* td-agent v4.xはEOLなのでfluent-package LTSに移行しよう!

